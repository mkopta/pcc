#!/usr/local/bin/python3.6
from pygal.maps.world import World


world = World()
world.title = 'Populations of Countries in North America'
world.add(
	'North America',
	{'ca': 34126000, 'us': 309349000, 'mx': 113423000}
)

world.render_to_file('na_populations.svg')
